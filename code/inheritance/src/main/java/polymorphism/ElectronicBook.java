package polymorphism;

public class ElectronicBook extends Book {

    private double numberBytes;

    
    public ElectronicBook(String title, String author, double numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    public double getNumberBytes() {
        return numberBytes;
    }


    public String toString() {
        String fromBase = super.toString() + " number of Bytes: " + this.numberBytes;
        return fromBase;
    }



}
