package polymorphism;

public class BookStore {

    public static void main(String[] args) {

        Book[] books = new Book[5];

        books[0] = new Book("Life of Michelle", "Michelle");
        books[1] = new ElectronicBook("Random ebook", "Rida", 110000);
        books[2] = new Book("Why is this so long", "reeeeda");
        books[3] = new ElectronicBook("Living in dawson", "Studen8", 90);
        books[4] = new ElectronicBook("How to make Swetha give you 100%", "Michelle Jr.", 9000);

        for(int i = 0; i < books.length; i++) {
            System.out.println(books[i]);
        }

        ElectronicBook b = (ElectronicBook)books[0];

        System.out.println(b.getNumberBytes());



    }
    
}
